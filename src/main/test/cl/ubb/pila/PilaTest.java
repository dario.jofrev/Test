package cl.ubb.pila;

import static org.junit.Assert.*;

import org.junit.Test;

public class PilaTest {
	
	@Test
	public void stackEsVacia() {
		Pila stack = new Pila();
		boolean vacia = stack.isEmpty();
		assertEquals(vacia,true);
		
	}

	@Test
	public void agregarNumeroUnoStackNoVacia(){
		Pila stack = new Pila();
		stack.push(1);
		boolean vacia = stack.isEmpty();
		assertEquals(vacia,false);
	}
	
	@Test
	public void agregarNumeroUnoYDosStackNoVacia(){
		Pila stack = new Pila();
		stack.push(1);
		stack.push(2);
		boolean vacia = stack.isEmpty();
		assertEquals(vacia,false);
	}
	
	@Test
	public void agregarNumeroUnoYDosTama˝oStackEsDos(){
		Pila stack = new Pila();
		stack.push(1);
		stack.push(2);
		int tamanoStack = stack.sizePila();
		assertEquals(tamanoStack,2);
	}
	@Test
	public void agregarNumeroUnoYHacerPopDevuelveNumeroUno(){
		Pila stack = new Pila();
		stack.push(1);
		int numero = stack.pop();
		assertEquals(numero, 1);	
	}
	@Test
	public void agregarNumeroUnoYDosHacerPopDevuelveNumeroDos(){
		Pila stack = new Pila();
		stack.push(1);
		stack.push(2);
		int numero = stack.pop();
		assertEquals(numero, 2);
	}
	@Test
	public void agregarNumeroTresYCuatroHacerPopDosVecesDevuelveCuatroYTres(){
		Pila stack = new Pila();
		stack.push(3);
		stack.push(4);
		int numero = stack.pop();
		assertEquals(numero, 4);
		numero = stack.pop();
		assertEquals(numero, 3);
	}
	
	@Test 
	public void agregarUnoHacerTopDevuelveUno(){
		Pila stack = new Pila();
		stack.push(1);
		int numero = stack.top();
		assertEquals(numero,1);
	}
	@Test
	public void agregarUnoYDosHacerTopDevuelveDos(){
		Pila stack = new Pila();
		stack.push(1);
		stack.push(2);
		int numero = stack.top();
		assertEquals(numero,2);
	}
	
}
