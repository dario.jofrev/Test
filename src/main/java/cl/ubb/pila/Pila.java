package cl.ubb.pila;

public class Pila {
	int nElementos;
	int a[];
	public Pila () {
		nElementos = 0;
		a = new int[300];
	}
	public boolean isEmpty(){
		if(nElementos==0){
			return true;
		}
		return false;
	}
	
	public void push(int numero){
		a[nElementos] = numero;
		nElementos++;
	}
	public int sizePila(){
		return nElementos;
	}
	public int pop(){
		nElementos--;
		return a[nElementos];
	}
	public int top(){
		return a[nElementos-1];
	}
	public static void main(String[] args) {
		

	}

}
